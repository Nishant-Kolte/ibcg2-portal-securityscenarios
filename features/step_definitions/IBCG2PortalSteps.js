/**
 * Created by Nishant-Kolte on 12/12/2017.
 */

var data = require('../../TestResources/IBCG2PortalData.js');
var objects = require(__dirname + '/../../repository/IBCG2PortalPages.js');

var LoginPage, HomePage,AdminPage;

function initializePageObjects(browser, callback) {
    var IBCG2Page = browser.page.IBCG2PortalPages();
    LoginPage = IBCG2Page.section.LoginPage;
    HomePage = IBCG2Page.section.HomePage;
    AdminPage = IBCG2Page.section.AdminPage;
    callback();
}


module.exports = function ()
{

  this.Given(/^User logs in to IBC application and lands on home page$/, function ()
      {
        browser=this;
          initializePageObjects(browser, function () {
              browser.maximizeWindow()
                  .deleteCookies()
                  .url(data.IBCG2URL);
              browser.timeoutsImplicitWait(data.longwait);
              LoginPage.waitForElementVisible('@Username',data.shortwait)
              .setValue('@Username',data.Username);
              browser.pause(1000);

              LoginPage.setValue('@Password',data.Password);
              browser.pause(1000);
              LoginPage.click('@Submitbutton')
          HomePage.assert.elementPresent('@Hometab');
          })

      }
    );

  this.When(/^User clicks on logout button$/, function ()
        {
            browser=this;
            initializePageObjects(browser, function () {
                HomePage.waitForElementVisible('@Logoutlink',data.shortwait);
                HomePage.click('@Logoutlink');
                browser.pause(2000);
                browser.acceptAlert();
                browser.pause(2000);
                LoginPage.assert.elementPresent('@Logoutwarning');
            })

        }
    );

  this.When(/^User clicks on back button of browser$/, function ()
        {
            browser=this;
            browser.back();
            browser.pause(2000);
        }
    );

  this.Then(/^User should not allow to access home page again$/, function ()
        {
            browser=this;
            initializePageObjects(browser, function () {

                // HomePage.assert.elementNotPresent('@Hometab');
            LoginPage.assert.urlContains("Login.tpz");
            })
        }
    );
    this.When(/^User try to login again with default credentials$/, function ()
    {
        browser=this;
                browser.maximizeWindow()
                .deleteCookies()
                .url(data.IBCG2URL);
            browser.timeoutsImplicitWait(data.longwait);
            LoginPage.waitForElementVisible('@Username',data.shortwait)
                .setValue('@Username',data.Defaultusername);
            browser.pause(1000);

            LoginPage.setValue('@Password',data.Defaultpassword);
            browser.pause(1000);
            LoginPage.click('@Submitbutton')

        });
    this.Then(/^User should get error message and not able to login$/, function ()
    {
        LoginPage.waitForElementVisible('@invalidcredswarning',data.shortwait);
        LoginPage.assert.elementPresent('@invalidcredswarning');
    })

    this.Given(/^IBCG2 login page is opened in web-browser$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.IBCG2URL);
            browser.timeoutsImplicitWait(data.longwait);
            LoginPage.waitForElementVisible('@Username', data.shortwait)
        })
    });

    this.Given(/^Try to access IBC login url in iframe via click-jacking snippet$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.clickjacking_path);
                //.url("C:/Users/Nishant-Kolte/Desktop/test.html")
            browser.timeoutsImplicitWait(data.longwait);
           })
    });

    this.Given(/^IBCG2 login page is opened via https transport protocol$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.IBCG2URL);
                LoginPage.waitForElementVisible('@Username', data.shortwait)
        })
    });

    this.When(/^User try to open IBCG2 login page via http transport protocol$/, function ()
    {
        browser = this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                  .url(data.IBCG2URL2);
        })
    });

    this.Given(/^IBCG2 non-vanity login page is opened in web-browser$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.IBCNVURL);
            browser.timeoutsImplicitWait(data.longwait);
            LoginPage.waitForElementVisible('@Username', data.shortwait)
        })
    });

    this.When(/^User enters invalid username and invalid password$/, function ()
            {
                browser=this;
                initializePageObjects(browser, function () {
                   LoginPage.setValue('@Username',data.invalidusername);
                   LoginPage.setValue('@Password',data.invalidpassword);
                    browser.pause(1000);
                    LoginPage.click('@Submitbutton')
                })

       });

    this.Then(/^User should get invalid username and password error message$/, function ()
        {
            browser=this;
            LoginPage.assert.elementPresent('@invalidcredswarning');

        });

    this.When(/^User enters valid username and invalid password$/, function ()
    {
        browser=this;
        initializePageObjects(browser, function () {
            LoginPage.setValue('@Username',data.Username);
            LoginPage.setValue('@Password',data.invalidpassword);
            browser.pause(1000);
            LoginPage.click('@Submitbutton')
        })
    });

    this.When(/^User enters invalid username and valid password$/, function ()
    {
        browser=this;
        browser.maximizeWindow()
            .deleteCookies()
            .url(data.IBCG2URL);
        initializePageObjects(browser, function () {
            LoginPage.setValue('@Username',data.invalidusername);
            LoginPage.setValue('@Password',data.Password);
            browser.pause(1000);
            LoginPage.click('@Submitbutton')
        })
    });
    this.When(/^User enters valid username and invalid password for 5 times$/, function ()
    {
        browser=this

        initializePageObjects(browser, function () {
            for (var i=1;i<=5;i++)
            {
                browser.url(data.IBCG2URL);
                LoginPage.setValue('@Username',data.username1);
                LoginPage.setValue('@Password',data.invalidpassword);
                LoginPage.click('@Submitbutton')
            }

        })

    });
    this.Then(/^User's account should get locked and warning message should be displayed on login page$/, function ()
    {
        browser=this;
        LoginPage.assert.elementPresent('@accountlockwarning');

    });

    this.When(/^User remains ideal for 20 mins and then clicks on wealth tab$/, function () {
        browser=this;
        browser.pause(1200000);
        initializePageObjects(browser, function () {
            HomePage.waitForElementVisible('@Wealthtab',1000);
            HomePage.click('@Wealthtab')
        })

    });

    this.Then(/^User should get re-directed to login page and timeout message should be displayed$/, function () {
        browser=this;
        initializePageObjects(browser, function () {
            LoginPage.waitForElementVisible('@timeoutmessage',5000);
            LoginPage.assert.elementPresent('@timeoutmessage');
        })

    });
    this.When(/^User opens URL of administration page$/, function () {
        browser=this;
        browser.url(data.adminURL);



        });

    this.When(/^User try to access health forms url without login$/, function () {
        browser=this;
        browser.url(data.HealthFormlink);



    });

        this.Then(/^Verify user should not see Employee Search link$/, function () {
            browser = this;
            initializePageObjects(browser, function () {
                AdminPage.assert.elementNotPresent('@EmployeeSearchlink');
            })

        });

    this.Then(/^URL should get changed from http to https$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            browser.assert.urlContains("https://qai.ibenefitcenter.com/");
        })

    });
    this.Then(/^User should not able to access the health form before login$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
            browser.assert.urlContains("login.tpz?ReturnUrl");
        })

    });
    this.When(/^User enters valid username and SQL query in password field$/, function ()
    {
        browser=this;
        initializePageObjects(browser, function () {
            LoginPage.setValue('@Username',data.Username);
            LoginPage.setValue('@Password',data.SQLInjection);
            browser.pause(1000);
            LoginPage.click('@Submitbutton')
        })
    });
    this.Then(/^Verify user should not see login page inside iframe$/, function () {
        browser = this;
        initializePageObjects(browser, function () {
          //  browser.frame(1);
            browser.pause(3000);
            LoginPage.assert.elementNotPresent('@Username');
       })

    });

}
