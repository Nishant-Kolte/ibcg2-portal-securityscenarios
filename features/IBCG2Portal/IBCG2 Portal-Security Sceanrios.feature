@Security_Scenarios_IBC
Feature: Security Scenarios for IBCG2 Portal

  @IBCG2_Session_Back-Tracking
  Scenario: To verify session back-tracking in IBC application
    Given User logs in to IBC application and lands on home page
    When User clicks on logout button
    And User clicks on back button of browser
    Then User should not allow to access home page again

  @IBCG2_Default_creds_login
  Scenario: To verify returning user is not able to login to IBC application with default credential
    Given User logs in to IBC application and lands on home page
    When User clicks on logout button
    And User try to login again with default credentials
    Then User should get error message and not able to login

  @IBCG2_Error_code
  Scenario: To verify error message when user enters invalid username or password
    Given IBCG2 login page is opened in web-browser
    When User enters invalid username and invalid password
    Then User should get invalid username and password error message
    When User enters valid username and invalid password
    Then User should get invalid username and password error message
    When User enters invalid username and valid password
    Then User should get invalid username and password error message

  @IBCG2_Weak_Lockout_mechanism
  Scenario: To verify that user's IBC account gets locked after making 5 consicutive invalid login attempts
    Given IBCG2 login page is opened in web-browser
    When User enters valid username and invalid password for 5 times
    Then User's account should get locked and warning message should be displayed on login page

  @IBCG2_Bypassing_Authorization_Schema
  Scenario: To verify ppt without admin/proxy group cannot access employee search for external proxy
    Given User logs in to IBC application and lands on home page
    When User opens URL of administration page
    Then Verify user should not see Employee Search link

  @IBCG2_Bypassing_Authentication_Schema
  Scenario: To verify that a ppt is not able to access the file/page of the application without login
    Given IBCG2 non-vanity login page is opened in web-browser
    When User try to access health forms url without login
    Then User should not able to access the health form before login

  @IBCG2_HTTP_STRICT_TRANSPORT_SECURITY
  Scenario: To verify http transport security for IBC application
    Given IBCG2 login page is opened via https transport protocol
    When User try to open IBCG2 login page via http transport protocol
    Then URL should get changed from http to https

  @IBCG2_SQL_Injection
  Scenario: To verify that the IBC application does not allow SQL injection
    Given IBCG2 login page is opened in web-browser
    When User enters valid username and SQL query in password field
    Then User should get invalid username and password error message

  @IBCG2_Click_jacking
  Scenario: To verfiy whether IBC opens in an iframe
    Given Try to access IBC login url in iframe via click-jacking snippet
    Then Verify user should not see login page inside iframe


  @IBCG2_Session_Timeout
  Scenario: To verify session timeout in IBC application
    Given User logs in to IBC application and lands on home page
    When User remains ideal for 20 mins and then clicks on wealth tab
    Then User should get re-directed to login page and timeout message should be displayed