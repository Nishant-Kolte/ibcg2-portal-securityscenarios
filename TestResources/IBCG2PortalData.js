/**
 * Created by Nishant-Kolte on 12/12/2017.
 */

module.exports= {

    IBCG2URL: "https://qai.ibenefitcenter.com/QAWORK/login.tpz",
    Username: "QAWORK1903",
    Password: "QAWORKpass1",

    //Scenario -2: default creds
    Defaultusername: "459061903",
    Defaultpassword: "061955",

    //Scenario -3: invalid UN or password error code
    invalidusername: "xyz12345",
    invalidpassword: "xyz12345",

    //weblockout:
    username1: "test2932",

    //URL of administration page-
    adminURL: "https://qai.ibenefitcenter.com/QAWORK/G2AdminLandingPage.tpz",

    //IBCG2 non-vanity url:
    IBCNVURL:  "https://qai.ibenefitcenter.com/login.tpz",

    //Forms link:
    HealthFormlink: "https://qai.ibenefitcenter.com/Attachments/Forms/Hiring%20Zone.pdf",

    //http Transport security
    IBCG2URL2: "http://qai.ibenefitcenter.com/QAWORK/login.tpz",

    //SQL Injection
    SQLInjection: "'1'='1'",

    //path of clickjacking snippet
clickjacking_path: "C:/IBCG2_Automation_training/IBCG2 Security Scenarios/TestResources/test.html",




    //waits
    longwait: 30000,
    shortwait: 10000,
}